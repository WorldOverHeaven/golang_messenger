# golang_messenger

## Сделать

Добавить показ предыдущих сообщений
Показать все сообщения после определенной даты

Сообщение 
Никнейм пользователя
Дата 
Тип
Контент (текст)

## Запуск

Запустить приложение можно, если установлен docker и docker-compose.  
Для этого необходимо выполнить следующие команды:  

cd deploy  
docker-compose up --build  

## html странички

http://localhost:8080/signup - регистрация   
http://localhost:8080/signin - авторизация

Пока не пройдена авторизация пользователя чат отдает 500 ошибку

http://localhost:8080/chat_html - чат

## запросы

http://localhost:8080/create_user - регистрация [POST]  
http://localhost:8080/login - авторизация [POST] 

Пока не пройдена авторизация пользователя чат отдает 500 ошибку

ws://localhost:8080/chat - чат



Это может занять довольно большое время, если необходимо установить отсутствующие образы

## Чат

Чат находится по адресу /chat  
Работает на технологии websocket

## Swagger

swagger находится в директории docs  

``` swagger
basePath: /
definitions:
  model.BadResponse:
    properties:
      error:
        type: string
    type: object
  model.CreateUserRequest:
    properties:
      email:
        type: string
      nick:
        type: string
      password:
        type: string
    type: object
  model.CreateUserResponse:
    properties:
      token:
        type: string
    type: object
  model.LoginRequest:
    properties:
      nick:
        type: string
      password:
        type: string
    type: object
  model.LoginResponse:
    properties:
      token:
        type: string
    type: object
host: localhost:8080
info:
  contact: {}
  description: API for app Simple Messenger
  title: Simple Messenger
  version: "1.0"
paths:
  /create_user:
    post:
      consumes:
      - application/json
      description: Create user
      parameters:
      - description: userInfo
        in: body
        name: userInfo
        required: true
        schema:
          $ref: '#/definitions/model.CreateUserRequest'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/model.CreateUserResponse'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/model.BadResponse'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/model.BadResponse'
      summary: Create user
  /login:
    post:
      consumes:
      - application/json
      description: Login user
      parameters:
      - description: loginInfo
        in: body
        name: loginInfo
        required: true
        schema:
          $ref: '#/definitions/model.LoginRequest'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/model.LoginResponse'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/model.BadResponse'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/model.BadResponse'
      summary: Login user
schemes:
- http
swagger: "2.0"

```
