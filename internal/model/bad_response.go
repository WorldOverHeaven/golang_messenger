package model

type BadResponse struct {
	Error string `json:"error"`
}
