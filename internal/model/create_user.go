package model

import "errors"

type CreateUserRequest struct {
	Nick     string `json:"nickname"`
	Password string `json:"password"`
	EMail    string `json:"email"`
}

func (c *CreateUserRequest) Validate() error {
	if len(c.Nick) < 3 {
		return errors.New("error in nick")
	}
	if len(c.Password) <= 5 {
		return errors.New("error in password")
	}
	return nil
}

type CreateUserResponse struct {
	Token string `json:"token"`
}
