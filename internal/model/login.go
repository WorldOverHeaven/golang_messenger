package model

type LoginRequest struct {
	Nick     string `json:"nickname"`
	Password string `json:"password"`
}

type LoginResponse struct {
	Token string `json:"token"`
}
