package file_handler

import (
	"github.com/labstack/echo/v4"
	"net/http"
)

// handler struct for declaring api methods
type handler struct {
	rootPath string
}

type Handler interface {
	GetSignUpHTML(ctx echo.Context) error
	GetSignInHTML(ctx echo.Context) error
	GetChat(ctx echo.Context) error
}

// NewHandler constructor for handler, user for code generation in wire
func NewHandler(rootPath string) Handler {
	return &handler{rootPath: rootPath}
}

func (h *handler) GetSignUpHTML(ctx echo.Context) error {
	return ctx.File(h.rootPath + "/internal/view/signup.html")
}

func (h *handler) GetSignInHTML(ctx echo.Context) error {
	return ctx.File(h.rootPath + "/internal/view/signin.html")
}

func (h *handler) GetChat(ctx echo.Context) error {
	return ctx.Render(http.StatusOK, "chat.html", ctx.Param("chat_name"))
}
