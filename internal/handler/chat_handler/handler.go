package chat_handler

import (
	"context"
	"errors"
	"fmt"
	"github.com/gorilla/websocket"
	"github.com/labstack/echo/v4"
	"golang_messenger/internal/controller/chat_controller"
	"golang_messenger/internal/controller/user_controller"
	"golang_messenger/internal/model"
	"golang_messenger/pkg/auth"
	"log"
	"net/http"
	"time"
)

type handler struct {
	upgrader          websocket.Upgrader
	chatSessionFabric chat_controller.ChatSessionFabric
	userController    user_controller.Controller
	authService       auth.Service
}

type Handler interface {
	WebsocketHandler(c echo.Context) error
	CreateUser(c echo.Context) error
	Login(c echo.Context) error
	Test(c echo.Context) error
}

func New(upgrader websocket.Upgrader, chatSessionFabric chat_controller.ChatSessionFabric, userController user_controller.Controller, authService auth.Service) Handler {
	return &handler{
		upgrader:          upgrader,
		chatSessionFabric: chatSessionFabric,
		userController:    userController,
		authService:       authService,
	}
}

func (h *handler) WebsocketHandler(c echo.Context) error {
	userID, ok := c.Get("user_id").(int64)
	if !ok {
		log.Println("user id parse failed")
		return errors.New("user id parse failed")
	}

	userNick, ok := c.Get("user_nick").(string)
	if !ok {
		log.Println("user nick parse failed")
		return errors.New("user nick parse failed")
	}

	peer, err := h.upgrader.Upgrade(c.Response(), c.Request(), nil)

	if err != nil {
		log.Println("websocket conn failed", err)
		return err
	}

	chatSession := h.chatSessionFabric.New(userID, c.Param("chat_name"), userNick, peer)
	chatSession.Start()
	return nil
}

func (h *handler) Test(c echo.Context) error {
	fmt.Println("a", c.QueryParam("a"))
	fmt.Println("b", c.QueryParam("b"))
	fmt.Println("c", c.QueryParam("c"))
	return c.String(http.StatusOK, "Hello")
}

// CreateUser godoc
// @Summary      Create user
// @Description  Create user
// @Accept       json
// @Produce      json
// @Param        userInfo   body      model.CreateUserRequest  true "userInfo"
// @Success      200  {object}  model.CreateUserResponse
// @Failure      400  {object}  model.BadResponse
// @Failure      500  {object}  model.BadResponse
// @Router       /create_user [post]
func (h *handler) CreateUser(c echo.Context) error {
	var request model.CreateUserRequest
	if err := c.Bind(&request); err != nil {
		code := http.StatusBadRequest
		log.Println("user bad request ", err)
		return c.JSON(code, model.BadResponse{Error: "user bad request" + err.Error()})
	}

	if err := request.Validate(); err != nil {
		code := http.StatusBadRequest
		log.Println("user bad request ", err)
		return c.JSON(code, model.BadResponse{Error: "user bad request" + err.Error()})
	}

	ctx := context.Background()
	userID, err := h.userController.CreateUser(ctx, request)
	if err != nil {
		code := http.StatusInternalServerError
		log.Println("internal server error", err)
		return c.JSON(code, model.BadResponse{Error: "internal server error" + err.Error()})
	}

	token, err := h.authService.CreateToken(userID, request.Nick)
	if err != nil {
		code := http.StatusInternalServerError
		log.Println("internal server error", err)
		return c.JSON(code, model.BadResponse{Error: "internal server error" + err.Error()})
	}

	cookie := new(http.Cookie)
	cookie.Name = "token"
	cookie.Value = token
	cookie.Expires = time.Now().Add(24 * time.Hour)
	c.SetCookie(cookie)

	return c.JSON(http.StatusOK, model.CreateUserResponse{Token: token})
}

// Login godoc
// @Summary      Login user
// @Description  Login user
// @Accept       json
// @Produce      json
// @Param        loginInfo   body      model.LoginRequest  true "loginInfo"
// @Success      200  {object}  model.LoginResponse
// @Failure      400  {object}  model.BadResponse
// @Failure      500  {object}  model.BadResponse
// @Router       /login [post]
func (h *handler) Login(c echo.Context) error {
	var request model.LoginRequest
	if err := c.Bind(&request); err != nil {
		code := http.StatusBadRequest
		log.Println("user bad request", err)
		return c.JSON(code, model.BadResponse{Error: "user bad request" + err.Error()})
	}

	ctx := context.Background()
	userID, err := h.userController.LoginUser(ctx, request)
	if err != nil {
		code := http.StatusInternalServerError
		log.Println("internal server error", err)
		return c.JSON(code, model.BadResponse{Error: "internal server error" + err.Error()})
	}

	token, err := h.authService.CreateToken(userID, request.Nick)
	if err != nil {
		code := http.StatusInternalServerError
		log.Println("internal server error", err)
		return c.JSON(code, model.BadResponse{Error: "internal server error" + err.Error()})
	}

	cookie := new(http.Cookie)
	cookie.Name = "token"
	cookie.Value = token
	cookie.Expires = time.Now().Add(24 * time.Hour)
	c.SetCookie(cookie)

	return c.JSON(http.StatusOK, model.LoginResponse{Token: token})
}

func (h *handler) CreateChat(c echo.Context) error {
	return nil
}
