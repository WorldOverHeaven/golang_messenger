package handler

import (
	"github.com/labstack/echo/v4"
	"golang_messenger/internal/handler/chat_handler"
	"golang_messenger/internal/handler/file_handler"
)

func SetupRoutes(e *echo.Echo, chatHandler chat_handler.Handler, fileHandler file_handler.Handler) {
	e.GET("/chat", chatHandler.WebsocketHandler)
	e.GET("/chat/:chat_name", chatHandler.WebsocketHandler)
	e.POST("/login", chatHandler.Login)
	e.POST("/create_user", chatHandler.CreateUser)
	e.GET("/", chatHandler.Test)

	e.GET("/signup", fileHandler.GetSignUpHTML)
	e.GET("/signin", fileHandler.GetSignInHTML)
	e.GET("/chat_html/:chat_name", fileHandler.GetChat)
}
