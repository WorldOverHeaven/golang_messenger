package peers_cache

import (
	"errors"
	"github.com/gorilla/websocket"
)

type cache struct {
	// key - chat_id
	// value - map[user_id]; value - websocket connection
	peers map[int64]map[int64]*websocket.Conn
}

type Cache interface {
	Set(chatID, userID int64, conn *websocket.Conn) error
	GetAllChatConnections(chatID int64) (map[int64]*websocket.Conn, error)
	Delete(chatID, userID int64) error
}

func New() Cache {
	return &cache{peers: make(map[int64]map[int64]*websocket.Conn)}
}

func (c *cache) Set(chatID, userID int64, conn *websocket.Conn) error {
	if _, ok := c.peers[chatID]; !ok {
		c.peers[chatID] = make(map[int64]*websocket.Conn)
	}
	c.peers[chatID][userID] = conn
	return nil
}

func (c *cache) GetAllChatConnections(chatID int64) (map[int64]*websocket.Conn, error) {
	val, ok := c.peers[chatID]
	if !ok {
		return nil, errors.New("has no chat_id")
	}
	return val, nil
}

func (c *cache) Delete(chatID, userID int64) error {
	delete(c.peers[chatID], userID)
	if len(c.peers[chatID]) == 0 {
		delete(c.peers, chatID)
	}
	return nil
}
