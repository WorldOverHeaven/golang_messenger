package chat_controller

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"github.com/gorilla/websocket"
	"golang_messenger/internal/database"
	"golang_messenger/internal/dto"
	"golang_messenger/internal/peers_cache"
	"log"
	"time"
)

type chatSessionFabric struct {
	cache peers_cache.Cache
	db    database.Database
}

type ChatSessionFabric interface {
	New(userID int64, chatName string, userName string, peer *websocket.Conn) *chatSession
}

func NewChatSessionFabric(cache peers_cache.Cache, db database.Database) ChatSessionFabric {
	return &chatSessionFabric{cache: cache, db: db}
}

// ChatSession represents a connected/active chat user
type chatSession struct {
	chatID   int64
	userID   int64
	userName string
	peer     *websocket.Conn

	cache peers_cache.Cache
	db    database.Database
}

// New returns a new ChatSession
func (c *chatSessionFabric) New(userID int64, chatName string, userName string, peer *websocket.Conn) *chatSession {
	chatID, err := c.chatNameToChatID(chatName)
	if err != nil {
		chatID = 1
		log.Println("chatNameToChatID error", err)
	}
	return &chatSession{userID: userID, peer: peer, chatID: chatID, userName: userName, cache: c.cache, db: c.db}
}

func (c *chatSessionFabric) chatNameToChatID(chatName string) (int64, error) {
	log.Println("chatNameToChatID chatName", chatName)
	if chatName == "" {
		return 1, nil
	}
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()
	id, err := c.db.SelectChatIDByTitle(ctx, chatName)
	if err == nil {
		return id, nil
	}
	if !errors.Is(err, sql.ErrNoRows) {
		log.Println(fmt.Sprintf("chatNameToChatID chatName: %s err: %e", chatName, err))
		return 1, err
	}
	id, err = c.db.CreateChat(ctx, dto.Chat{
		Title:     chatName,
		CreatedAt: time.Now(),
	})
	log.Println(fmt.Sprintf("chatNameToChatID chat created chatName: %s", chatName))
	if err != nil {
		log.Println(fmt.Sprintf("chatNameToChatID chatName: %s err: %e", chatName, err))
		return 1, err
	}
	return id, nil
}

const (
	usernameHasBeenTaken = "username %s is already taken. please retry with a different name"
	retryMessage         = "failed to connect. please try again"
	welcome              = "Welcome %s!"
	joined               = "Присоединился к чату!"
	chat                 = "%s: %s"
	left                 = "Покинул чат!"
)

// Start starts the chat by reading messages sent by the peer and broadcasting the to redis pub-sub channel
func (s *chatSession) Start() {
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	err := s.db.AddUserToChat(ctx, s.chatID, s.userID, time.Now())
	if err != nil {
		log.Println("failed to set database", err)
		return
	}

	err = s.cache.Set(s.chatID, s.userID, s.peer)
	if err != nil {
		log.Println("failed to set cache", err)
		return
	}

	peers, err := s.cache.GetAllChatConnections(s.chatID)
	if err != nil {
		log.Println("failed to get cache", err)
		return
	}

	s.notifyPeers(joined, peers)

	/*
		this go-routine will exit when:
		(1) the user disconnects from chat manually
		(2) the app is closed
	*/
	go func() {
		log.Println("user joined", s.userID)
		defer s.disconnect()
		for {
			err = s.readMessage()
			if err != nil {
				return
			}
		}
	}()
}

func (s *chatSession) readMessage() error {
	_, msg, err := s.peer.ReadMessage()

	if err != nil {
		var closeError *websocket.CloseError
		ok := errors.As(err, &closeError)
		if ok {
			log.Println("connection closed by user")
		}

		peers, errCache := s.cache.GetAllChatConnections(s.chatID)
		if errCache != nil {
			log.Println("failed to get cache", errCache)
		}

		s.notifyPeers(left, peers)

		return err
	}

	content := string(msg)
	if content == "" {
		return nil
	}

	message := dto.Message{
		ID:        0,
		ChatID:    s.chatID,
		UserID:    s.userID,
		Content:   content,
		CreatedAt: time.Now(),
	}

	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	_, err = s.db.CreateMessage(ctx, message)
	if err != nil {
		log.Println("failed to create message", err)
	}

	peers, err := s.cache.GetAllChatConnections(s.chatID)
	if err != nil {
		log.Println("failed to get cache", err)
	}

	s.notifyPeers(string(msg), peers)

	return nil
}

func (s *chatSession) notifyPeers(msg string, peers map[int64]*websocket.Conn) {
	for _, peer := range peers {
		err := peer.WriteMessage(websocket.TextMessage, []byte(s.userName+" : "+msg))
		if err != nil {
			log.Println("failed to write message", err)
		}
	}
}

// Invoked when the user disconnects (websocket connection is closed). It performs cleanup activities
func (s *chatSession) disconnect() {
	err := s.cache.Delete(s.chatID, s.userID)
	if err != nil {
		log.Println("failed to delete from cache", err)
	}

	err = s.peer.Close()
	if err != nil {
		log.Println("failed to peer close", err)
	}

	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	err = s.db.DeleteUserFromChat(ctx, s.chatID, s.userID)
	if err != nil {
		log.Println("failed to delete user from chat", err)
	}
}
