package user_controller

import (
	"context"
	"crypto/sha256"
	"fmt"
	"golang.org/x/crypto/bcrypt"
	"golang_messenger/internal/database"
	"golang_messenger/internal/dto"
	"golang_messenger/internal/model"
	"golang_messenger/pkg/create_random_string"
	"hash"
	"time"
)

type controller struct {
	saltLen int
	db      database.Database
	creator create_random_string.Creator
	hasher  hash.Hash
}

type Controller interface {
	CreateUser(ctx context.Context, request model.CreateUserRequest) (int64, error)
	LoginUser(ctx context.Context, request model.LoginRequest) (int64, error)
}

func New(db database.Database, creator create_random_string.Creator) Controller {
	return &controller{db: db, creator: creator, saltLen: 10, hasher: sha256.New()}
}

func (c *controller) CreateUser(ctx context.Context, request model.CreateUserRequest) (int64, error) {
	user := dto.User{
		ID:        0,
		Nick:      request.Nick,
		Password:  "",
		EMail:     request.EMail,
		Salt:      c.creator.RandomString(c.saltLen),
		CreatedAt: time.Now(),
	}

	password, err := hashPassword(request.Password, user.Salt)
	if err != nil {
		return 0, err
	}

	user.Password = password

	return c.db.CreateUser(ctx, user)
}

func (c *controller) LoginUser(ctx context.Context, request model.LoginRequest) (int64, error) {
	user, err := c.db.SelectUserByNick(ctx, request.Nick)
	if err != nil {
		return 0, err
	}

	password := request.Password + user.Salt

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)); err != nil {
		return 0, fmt.Errorf("incorrect nick or password %w", err)
	}

	fmt.Println(user)

	return user.ID, nil
}

// Hash password using the bcrypt hashing algorithm
func hashPassword(password, salt string) (string, error) {
	// Convert password string to byte slice
	var passwordBytes = []byte(password + salt)

	// Hash password with bcrypt's default cost
	hashedPasswordBytes, err := bcrypt.
		GenerateFromPassword(passwordBytes, bcrypt.DefaultCost)

	return string(hashedPasswordBytes), err
}
