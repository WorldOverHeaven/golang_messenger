package database

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"golang_messenger/internal/dto"
	"time"

	_ "github.com/lib/pq"
)

type database struct {
	client *sql.DB
}

type Database interface {
	CreateUser(ctx context.Context, user dto.User) (int64, error)
	SelectUserById(ctx context.Context, id int64) (dto.User, error)
	CreateChat(ctx context.Context, chat dto.Chat) (int64, error)
	SelectChatTitleByID(ctx context.Context, id int64) (string, error)
	SelectChatIDByTitle(ctx context.Context, title string) (int64, error)
	AddUserToChat(ctx context.Context, chatID, userID int64, cratedAt time.Time) error
	DeleteUserFromChat(ctx context.Context, chatID, userID int64) error
	CreateMessage(ctx context.Context, message dto.Message) (int64, error)
	SelectUserByNick(ctx context.Context, nick string) (dto.User, error)
	Ping() error
}

func New(config Config) (Database, error) {
	connInfo := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		config.Host, config.Port, config.User, config.Password, config.Database,
	)
	client, err := sql.Open("postgres", connInfo)

	if err != nil {
		return nil, err
	}

	return &database{client: client}, nil
}

func (db *database) Ping() error {
	_, err := db.client.Exec(ping)
	return err
}

func (db *database) CreateUser(ctx context.Context, user dto.User) (int64, error) {
	var id int64
	row := db.client.QueryRowContext(ctx, createUser, user.Nick, user.Password, user.EMail, user.Salt, user.CreatedAt)

	err := row.Scan(&id)

	if err != nil {
		return 0, fmt.Errorf("create user error %w", err)
	}

	return id, nil
}

func (db *database) SelectUserById(ctx context.Context, id int64) (dto.User, error) {
	row := db.client.QueryRowContext(ctx, selectUserByID, id)

	user := dto.User{
		ID: id,
	}

	err := row.Scan(&user.Nick, &user.Password, &user.Salt, &user.CreatedAt)
	if err != nil {
		return dto.User{}, fmt.Errorf("select user error %w", err)
	}

	return user, nil
}

func (db *database) SelectUserByNick(ctx context.Context, nick string) (dto.User, error) {
	row := db.client.QueryRowContext(ctx, selectUserByNick, nick)

	user := dto.User{
		Nick: nick,
	}

	err := row.Scan(&user.ID, &user.Password, &user.EMail, &user.Salt, &user.CreatedAt)
	if err != nil {
		return dto.User{}, fmt.Errorf("select user error %w", err)
	}

	return user, nil
}

func (db *database) CreateChat(ctx context.Context, chat dto.Chat) (int64, error) {
	var id int64
	row := db.client.QueryRowContext(ctx, createChat, chat.Title, chat.CreatedAt)

	err := row.Scan(&id)

	if err != nil {
		return 0, fmt.Errorf("create chat error %w", err)
	}

	return id, nil
}

func (db *database) SelectChatTitleByID(ctx context.Context, id int64) (string, error) {
	var title string
	row := db.client.QueryRowContext(ctx, selectChatTitleByID, id)

	err := row.Scan(&title)
	if err != nil {
		return "", fmt.Errorf("select chat title by id error %w", err)
	}

	return title, nil
}

func (db *database) SelectChatIDByTitle(ctx context.Context, title string) (int64, error) {
	var id int64
	row := db.client.QueryRowContext(ctx, selectChatIDByTitle, title)

	err := row.Scan(&id)
	if errors.Is(err, sql.ErrNoRows) {
		return 0, err
	}
	if err != nil {
		return 0, fmt.Errorf("select chat title by id error %w", err)
	}

	return id, nil
}

func (db *database) AddUserToChat(ctx context.Context, chatID, userID int64, createdAt time.Time) error {
	_, err := db.client.ExecContext(ctx, addUserToChat, chatID, userID, createdAt)
	if err != nil {
		return fmt.Errorf("add user to chat error %w", err)
	}
	return nil
}

func (db *database) DeleteUserFromChat(ctx context.Context, chatID, userID int64) error {
	_, err := db.client.ExecContext(ctx, deleteUserFromChat, chatID, userID)
	if err != nil {
		return fmt.Errorf("delete user from chat error %w", err)
	}
	return nil
}

func (db *database) CreateMessage(ctx context.Context, message dto.Message) (int64, error) {
	var id int64
	row := db.client.QueryRowContext(ctx, createMessage, message.ChatID, message.UserID, message.Content, message.CreatedAt)

	err := row.Scan(&id)

	if err != nil {
		return 0, fmt.Errorf("create message error %w", err)
	}

	return id, nil
}
