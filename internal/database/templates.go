package database

const (
	ping = `
	SELECT 1 FROM users;
`
	createUser = `
	INSERT INTO users(nick, password, email, salt, created_at)
	VALUES ($1, $2, $3, $4, $5) RETURNING id;
`
	selectUserByID = `
	SELECT nick, password, email, salt, created_at FROM users
	WHERE id=$1;
`
	selectUserByNick = `
	SELECT id, password, email, salt, created_at FROM users
	WHERE nick=$1;
`
	createChat = `
	INSERT INTO chats(title, created_at)
	VALUES ($1, $2) RETURNING id;
`
	selectChatTitleByID = `
	SELECT title FROM chats
	WHERE id=$1
`
	selectChatIDByTitle = `
	SELECT id FROM chats
	WHERE title=$1
`
	addUserToChat = `
	INSERT INTO users_chats(chat_id, user_id, created_at)
	VALUES ($1, $2, $3);
`
	deleteUserFromChat = `
	DELETE FROM users_chats WHERE user_id = $1 AND chat_id = $2;
`
	createMessage = `
	INSERT INTO messages(chat_id, user_id, content, created_at)
	VALUES ($1, $2, $3, $4) RETURNING id;
`
)
