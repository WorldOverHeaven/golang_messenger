package dto

import "time"

type UsersChats struct {
	UserID    int64
	ChatID    int64
	CreatedAt time.Time
}
