package dto

import "time"

type Chat struct {
	ID        int64
	Title     string
	CreatedAt time.Time
}
