package dto

import "time"

type User struct {
	ID        int64
	Nick      string
	Password  string
	EMail     string
	Salt      string
	CreatedAt time.Time
}
