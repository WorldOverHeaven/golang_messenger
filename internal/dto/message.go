package dto

import "time"

type Message struct {
	ID        int64
	ChatID    int64
	UserID    int64
	Content   string
	CreatedAt time.Time
}
