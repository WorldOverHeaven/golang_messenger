package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"github.com/gorilla/websocket"
	"github.com/heetch/confita"
	"github.com/heetch/confita/backend/env"
	"github.com/heetch/confita/backend/file"
	"github.com/labstack/echo/v4"
	"github.com/swaggo/echo-swagger"
	"golang_messenger/internal/config"
	"golang_messenger/internal/controller/chat_controller"
	"golang_messenger/internal/controller/user_controller"
	"golang_messenger/internal/database"
	"golang_messenger/internal/dto"
	"golang_messenger/internal/handler"
	"golang_messenger/internal/handler/chat_handler"
	"golang_messenger/internal/handler/file_handler"
	"golang_messenger/internal/peers_cache"
	"golang_messenger/pkg/auth"
	"golang_messenger/pkg/create_random_string"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"time"

	_ "golang_messenger/docs"
)

var rootPath string

func init() {
	flag.StringVar(&rootPath, "rootPath", "", "root folder")
	flag.Parse()
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

const port = "8080"

// @title Simple Messenger
// @version 1.0
// @description API for app Simple Messenger
// @host localhost:8080
// @basePath /
// @schemes http
func main() {
	cfg, err := setupCfg()
	if err != nil {
		log.Fatal("config parse error")
	}

	db, err := database.New(cfg.Postgres)
	if err != nil {
		log.Fatal("database open error")
	}

	err = db.Ping()
	if err != nil {
		log.Fatal("database ping error")
	}

	ctx := context.Background()

	_, err = db.CreateChat(ctx, dto.Chat{
		ID:        1,
		Title:     "Title",
		CreatedAt: time.Now(),
	})
	if err != nil {
		log.Fatal("database ping error")
	}

	cache := peers_cache.New()

	chatSessionFabric := chat_controller.NewChatSessionFabric(cache, db)

	creator := create_random_string.New()

	userController := user_controller.New(db, creator)

	authService := auth.New("secret")

	chatHandler := chat_handler.New(upgrader, chatSessionFabric, userController, authService)

	fileHandler := file_handler.NewHandler(rootPath)

	e := echo.New()

	t := &Template{
		templates: template.Must(template.ParseGlob(rootPath + "/internal/view/*.html")),
	}

	e.Renderer = t

	e.GET("/swagger/*", echoSwagger.WrapHandler)

	e.Use(Logger)

	e.Use(CreateJwtMiddleware(authService))

	handler.SetupRoutes(e, chatHandler, fileHandler)

	start(e)
}

type Template struct {
	templates *template.Template
}

func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}

func start(e *echo.Echo) {
	go func() {
		if err := e.Start(":" + port); err != nil && !errors.Is(err, http.ErrServerClosed) {
			e.Logger.Fatal("shutting down the server")
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}
}

func setupCfg() (config.Config, error) {
	ctx := context.Background()

	var cfg config.Config

	err := confita.NewLoader(
		file.NewBackend(fmt.Sprintf("%s/deploy/default.yaml", rootPath)),
		env.NewBackend(),
	).Load(ctx, &cfg)

	if err != nil {
		return config.Config{}, err
	}

	envDocker := os.Getenv("ENV")
	if envDocker != "docker" {
		cfg.Postgres.Host = "localhost"
	}

	return cfg, nil
}

func Logger(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		start := time.Now()

		if err := next(c); err != nil {
			c.Error(err)
		}

		log.Printf(
			"%s %s\n",
			c.Path(),
			time.Since(start),
		)

		fmt.Println("cookies", c.Cookies())

		return nil
	}
}

func CreateJwtMiddleware(service auth.Service) func(next echo.HandlerFunc) echo.HandlerFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			if c.Path() == "/create_user" ||
				c.Path() == "/login" ||
				c.Path() == "/signin" ||
				c.Path() == "/signup" ||
				strings.HasPrefix(c.Path(), "/swagger/") {
				if err := next(c); err != nil {
					c.Error(err)
				}
				return nil
			}

			token := strings.TrimPrefix(c.Request().Header.Get("Authorization"), "Bearer ")

			if token == "" {
				cookies := c.Cookies()
				for _, cookie := range cookies {
					if cookie.Name == "token" {
						token = cookie.Value
					}
				}
			}

			userID, userNick, err := service.AuthUser(token)
			if err != nil {
				log.Println("auth error:", err)
				return fmt.Errorf("auth error: %w", err)
			}

			c.Set("user_id", userID)
			c.Set("user_nick", userNick)

			if err := next(c); err != nil {
				c.Error(err)
			}

			return nil
		}
	}
}
