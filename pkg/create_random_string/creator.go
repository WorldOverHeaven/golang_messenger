package create_random_string

import "math/rand"

type creator struct {
}

type Creator interface {
	RandomString(n int) string
}

func New() Creator {
	return &creator{}
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func (c *creator) RandomString(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}
