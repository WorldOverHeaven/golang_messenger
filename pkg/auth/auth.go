package auth

import (
	"fmt"
	"github.com/golang-jwt/jwt"
	"strconv"
)

type Service interface {
	CreateToken(userID int64, userName string) (string, error)
	AuthUser(tokenString string) (int64, string, error)
}

type service struct {
	secret []byte
}

func New(secret string) Service {
	return &service{secret: []byte(secret)}
}

func (s *service) CreateToken(userID int64, userName string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"userID":   strconv.FormatInt(userID, 10),
		"userName": userName,
	})

	return token.SignedString(s.secret)
}

func (s *service) AuthUser(tokenString string) (int64, string, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return s.secret, nil
	})

	if err != nil {
		return 0, "", fmt.Errorf("parse error %w", err)
	}

	claims, ok := token.Claims.(jwt.MapClaims)

	if !(ok && token.Valid) {
		return 0, "", fmt.Errorf("claim missing")
	}

	str, ok := claims["userID"].(string)
	if !ok {
		return 0, "", fmt.Errorf("claim parsing failed")
	}
	userID, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		return 0, "", fmt.Errorf("claim parsing failed")
	}

	userName, ok := claims["userName"].(string)
	if !ok {
		return 0, "", fmt.Errorf("claim parsing failed")
	}

	return userID, userName, nil
}
