CREATE TABLE IF NOT EXISTS users (
    id SERIAL PRIMARY KEY,
    nick TEXT,
    password TEXT,
    email TEXT,
    salt TEXT,
    created_at timestamp
);

ALTER TABLE "users" ADD CONSTRAINT "users_unique" UNIQUE ("nick");

CREATE TABLE IF NOT EXISTS chats (
    id SERIAL PRIMARY KEY,
    title TEXT,
    created_at timestamp
);

ALTER TABLE "chats" ADD CONSTRAINT "title_unique" UNIQUE ("title");


CREATE TABLE IF NOT EXISTS messages (
    id SERIAL PRIMARY KEY,
    chat_id integer,
    user_id integer,
    content TEXT,
    created_at timestamp
);

-- ALTER TABLE "messages" ADD CONSTRAINT "messages_fk0" FOREIGN KEY ("user_id") REFERENCES "users"("id");
-- ALTER TABLE "messages" ADD CONSTRAINT "messages_fk1" FOREIGN KEY ("chat_id") REFERENCES "chats"("id");
-- ALTER TABLE "messages" ADD CONSTRAINT "messages_unique" UNIQUE ("user_id", "chat_id");


CREATE TABLE IF NOT EXISTS users_chats (
    chat_id integer,
    user_id integer,
    created_at timestamp
);
--
-- ALTER TABLE "users_chats" ADD CONSTRAINT "users_chats_fk0" FOREIGN KEY ("user_id") REFERENCES "users"("id");
-- ALTER TABLE "users_chats" ADD CONSTRAINT "users_chats_fk1" FOREIGN KEY ("chat_id") REFERENCES "chats"("id");
-- ALTER TABLE "users_chats" ADD CONSTRAINT "users_chats_unique" UNIQUE ("user_id", "chat_id");

